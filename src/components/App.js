import React from 'react'
import './app.css'

import Options from './Options'
import Game from './Game'

function App () {
  return (
    <div className="flex-group app-content">
      <Options />
      <Game />
    </div>
  )
}

export default App
