import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Row from './Row'
import Cell from './Cell'

const Game = class extends React.Component {
  static propTypes = {
    size: PropTypes.number.isRequired,
  }

  render () {
    const { size } = this.props

    const rows = Array.from({ length: size }, (_, i) => i)

    return (
      <div>
        {rows.map((rn) =>
          <Row key={rn}>
            {rows.map((i) => {
              const id = rn * size + i
              return <Cell key={id} id={id} />
            })}
          </Row>
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  size: state.size
})

export default connect(mapStateToProps)(Game)
