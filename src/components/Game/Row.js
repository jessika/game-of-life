import React from 'react'

export default React.memo((props) =>
  <div className="flex-group">
    {props.children}
  </div>
)
