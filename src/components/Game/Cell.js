import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { live, dead } from '../../Constants'

const Cell = class extends React.PureComponent {
  static propTypes = {
    status: PropTypes.oneOf([live, dead]),
    id: PropTypes.number.isRequired,
  }

  render () {
    return (
      <div className={`cell ${this.props.status === live ? 'cell--active' : ''}`} />
    )
  }
}

const mapStateToProps = (state, props) =>
  ({ status: state.universe[props.id] })

export default connect(mapStateToProps)(Cell)
