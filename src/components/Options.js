import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Select, Input, Label } from 'semantic-ui-react'

import actions from '../actions/actionCreators'

const Options = class extends React.PureComponent {
  static propTypes = {
    size: PropTypes.number.isRequired,
    seedSize: PropTypes.number.isRequired,
    tick: PropTypes.number.isRequired,
  }

  state = {
    started: false,
    size: this.props.size,
  }

  _timer = null

  _handleStart = () => {
    this._timer = setInterval(
      () => this.props.dispatch(actions.nextTick()),
      this.props.tick,
    )
    this.setState({ started: true })
  }

  _handleTick = () => {
    this.props.dispatch(actions.nextTick())
  }

  _handleStop = () => {
    clearInterval(this._timer)
    this.setState({ started: false })
  }

  _handleSetSeed = () => {
    this.props.dispatch(actions.setSeed(this.state.size))
  }

  _handleSeedSize = (e, target) => {
    const value = parseInt(target.value, 10)
    this.props.dispatch(actions.setSeedSize(value))
  }

  _handleSize = (e, target) => {
    const value = parseInt(target.value, 10)
    this.setState({ size: value })
  }

  render () {
    return (
      <div className="flex-group topbar">
        <div className="flex-group settings">
          <Input
            labelPosition="right"
            type="range"
            min={500}
            max={2000}
            value={this.props.seedSize}
            onChange={this._handleSeedSize}
            disabled={this.state.started}
          >
          <Label>Seed size</Label>
            <input />
            <Label>{this.props.seedSize}</Label>
          </Input>
          <Input
            labelPosition="right"
            type="range"
            min={10}
            max={120}
            step={20}
            value={this.state.size}
            onChange={this._handleSize}
            disabled={this.state.started}
          >
          <Label>Game size</Label>
            <input />
            <Label>{this.state.size} x {this.state.size}</Label>
          </Input>
        </div>
        <div>
          <Button
            disabled={this.state.started}
            onClick={this._handleSetSeed}
          >
            New Seed
          </Button>
          <Button
            active={this.state.started}
            onClick={this._handleStart}
          >
            Start
          </Button>
          <Button
            disabled={!this.state.started}
            onClick={this._handleStop}
          >
            Stop
          </Button>
          <Button
            disabled={this.state.started}
            onClick={this._handleTick}
          >
            Tick
          </Button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  size: state.size,
  seedSize: state.seedSize,
  tick: state.tick,
})

export default connect(mapStateToProps)(Options)
