import {
  sum,
  emptyUniverse,
  cellState,
  nLiveNeighbours,
  nextTick,
  getSeed,
} from './index'

const size = 10

const deadUniverse = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
const liveUniverse = [0, 0, 1, 1, 1, 0, 1, 0, 0, 0]
const liveUniverse2 = [0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
const liveUniverseL = [
  0, 0, 1, 1, 1, 0, 1, 0, 0, 0,
  0, 0, 1, 1, 1, 0, 1, 0, 0, 0,
  0, 0, 1, 1, 1, 0, 1, 0, 0, 0
]

describe('empty universe', () => {
  it('creates an array with only 0s', () => {
    expect(emptyUniverse(3)).toEqual([0,0,0,0,0,0,0,0,0])
  })
})

describe('sum', () => {
  it('sums up values in an array, counting undefined as 0', () => {
    expect(
      sum([undefined,undefined,undefined,1,1,undefined,undefined,undefined])
    ).toBe(2)
  })
})

describe('cellState', () => {
  it('calculates next state from nr of alive neighbours', () => {
    expect(cellState(0, 1)).toBe(0)
    expect(cellState(0, 2)).toBe(0)
    expect(cellState(0, 3)).toBe(1)
    expect(cellState(0, 4)).toBe(0)
    expect(cellState(1, 0)).toBe(0)
    expect(cellState(1, 2)).toBe(1)
    expect(cellState(1, 3)).toBe(1)
    expect(cellState(1, 4)).toBe(0)
  })
})

describe('nLiveNeighbours', () => {
  it('counts sum of neighbours\' values', () => {
    expect(nLiveNeighbours(0, deadUniverse, size)).toBe(0)
    expect(nLiveNeighbours(13, liveUniverseL, size)).toBe(8)
  })
})

describe('nextTick',() => {
  it('returns next state from current universe', () => {
    expect(nextTick(deadUniverse)).toStrictEqual(deadUniverse)
    expect(nextTick(liveUniverse)).toStrictEqual(liveUniverse2)
  })
})

describe('getSeed', () => {
  it('sets initial state with definied nr of active cells', () => {
    const seed = getSeed(20, size)
    expect(seed.length).toEqual(size * size)
    expect(sum(seed)).toBe(20)
  })
})
