import { live, dead } from '../Constants'

export const emptyUniverse = (size) =>
  Array.from({ length: size * size }, () => 0)

/**
* Returns next cell state by number of live neighbors
* - Any live cell with fewer than two live neighbours dies, as if by underpopulation.
* - Any live cell with two or three live neighbours lives on to the next generation.
* - Any live cell with more than three live neighbours dies, as if by overpopulation.
* - Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
*/
export const cellState = (state, nliveNgbr) =>
  state === dead && nliveNgbr === 3 ? live
    : state === live && (nliveNgbr === 2 || nliveNgbr === 3) ? live
    : dead

/*
* Sums up the vlaues in an array, treating `undefined` as 0
*/
export const sum = (arr) =>
  arr.reduce((acc, v) => acc + (v || 0), 0)

/**
* Returns all 8 neightbors for a cell
* @param {Integer} cell The cell index
* @param {Array<0/1>} universe The current state of all cells
* @param {Integer} size The size of the universe (lenght of a row or column)
* @returns {Integer} The sum of the neighbours values
*/
export const nLiveNeighbours = (cell, universe, size) => {
  const neighbours = [
    universe[cell - size - 1],
    universe[cell - size],
    universe[cell - size + 1],
    universe[cell - 1],
    universe[cell + 1],
    universe[cell + size - 1],
    universe[cell + size],
    universe[cell + size + 1],
  ]
  return sum(neighbours)
}

/**
* Returns the new universe from the current universe
*/
export const nextTick = (universe) => {
  const size = Math.sqrt(universe.length)
  return universe.map((cell, index) =>
    cellState(cell, nLiveNeighbours(index, universe, size)))
}

/**
* Returns an inital state by randomly assigning live cells
* @param {Integer} nrAlive Nr of live cells expected
* @param {Integer} size universe row/column length
* @returns {Array<0/1>} A new universe of defined size
*/
export const getSeed = (nLive, size) => {
  const uLength = size * size
  const init = emptyUniverse(size)
  while (nLive > 0) {
    init[Math.floor(Math.random() * uLength  + 1)] = 1
    nLive --
  }
  return init
}
