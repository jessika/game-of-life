/**
* Creates an action factory of a given type with given properties
* @param {string} type The action type in CAPS_CASE
* @param {Array<string>} argNames The name of the properties associated with this action
* @returns {Function} The function to be invoked when calling `dispatch`
*
* @example
* makeActionCreator('TYPE_A', ['a', 'b'])(123, 321)
* // { type: 'TYPE_A', a: 123, b: 321 }
*
* TODO: Checks when making and calling action creator
* TODO: Handle object as argNames
*/
export function makeActionCreator (type, argNames) {
  return (...args) => {
    const action = { type }

    if (argNames) {
      argNames.forEach((argName, index) => {
        action[argName] = args[index]
      })
    }

    return action
  }
}

/**
* Adds the action type and action creator on the provided exports object
* @param {Object} exports The exports object
* @param {string} typeCaps The action type in CAPS_CASE
* @param {Array<string>} argNames The name of the properties associated with this action
*/
export function registerAction (exports, typeCaps, argNames) {
 const typeCamel = capsToCamel(typeCaps)

 exports[typeCamel] = makeActionCreator(typeCaps, argNames)
 exports[typeCaps] = typeCaps
}

/**
* Converts a string from CAPS_CASE to camelCase
* @param {string} string
* @returns {string}
*/
export const capsToCamel = (string) =>
  string.toLowerCase().replace(/([_][a-z])/ig,
    ($1) => $1.toUpperCase().replace('_', ''))
