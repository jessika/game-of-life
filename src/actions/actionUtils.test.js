import { makeActionCreator, registerAction, capsToCamel } from './actionUtils'

describe('capsToCamel', () => {
  it('converts caps case to camel case', () => {
    expect(capsToCamel('SOME_CAPS')).toBe('someCaps')
    expect(capsToCamel('SOME_CAPS_CASE')).toBe('someCapsCase')
  })
})


describe('makeActionCreator', () => {
  it('returns an action factory', () => {
    const actionCreator = makeActionCreator('ACTION_TYPE', ['arg1'])
    const action = actionCreator(1)
    expect(action).toStrictEqual({ type: 'ACTION_TYPE', arg1: 1 })
  })
})

describe('registerAction', () => {
  it('adds action type to export', () => {
    const actions = {}
    registerAction(actions, 'TEST')
    expect(actions).toMatchObject({ TEST: 'TEST' })
  })
})
