import { registerAction } from './actionUtils'

const actions = {}

registerAction(actions, 'NEXT_TICK')
registerAction(actions, 'SET_SEED_SIZE', ['seedSize'])
registerAction(actions, 'SET_SEED', ['size'])
registerAction(actions, 'RESET')

export default actions
