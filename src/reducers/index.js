import actions from '../actions/actionCreators'
import * as api from '../api'

const initialSize = 100
const initalSeedSize = 500
const initialTickMs = 1000

const intialState = {
  size: initialSize,
  seedSize: initalSeedSize,
  tick: initialTickMs,
  universe: api.getSeed(initalSeedSize, initialSize),
}

function reducer (state = intialState, action) {
  switch (action.type) {
    case actions.SET_SEED_SIZE:
      return {
        ...state,
        seedSize: action.seedSize,
      }

    case actions.SET_SEED: {
      return {
        ...state,
        size: action.size,
        universe: api.getSeed(state.seedSize, action.size),
      }
    }

    case actions.NEXT_TICK: {
      return {
        ...state,
        universe: api.nextTick(state.universe),
      }
    }

    default: {
      console.warn(`Unhandled action ${action.type}`)
      return state
    }
  }
}

export default reducer
