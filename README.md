# Conway's Game of Life
The universe of the Game of Life is an infinite, two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead.
Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent.

At each step in time, the following transitions occur:

- Any live cell with fewer than two live neighbours dies, as if by underpopulation.
- Any live cell with two or three live neighbours lives on to the next generation.
- Any live cell with more than three live neighbours dies, as if by overpopulation.
- Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

The initial pattern constitutes the seed of the system.

https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

# Development
`yarn start-dev` in root folder

`yarn test` to run .test.js files and watch for changes

# Feature improvements
- Edges wrap around to the other side
- More seed options, such as distribution of live cells
- Identify known patters and color them
- Statistics: #alive/dead, patters, generations lived, largest/smallest gen
- Graph stats
- Keep history of games run
- Keep track of 'dead areas', if a cell hasn't been updated during the two
previous ticks, it doesn't need updating
